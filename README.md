
# Node Authentication App

## Overview

This is a simple Node.js authentication app using Express, Mongoose, and EJS for rendering views.

## Prerequisites

- Node.js installed
- MongoDB Atlas account or a local MongoDB instance

## Setup

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/your-repo.git
   cd your-repo


Install dependencies:

bash
Copy code
npm install
Configuration:

Create a config/default.json file with the following structure:

json
Copy code
{
  "MONGODB_URI": "YOUR_MONGODB_URI"
}
Replace "YOUR_MONGODB_URI" with your actual MongoDB URI.

Start the application:

bash
Copy code
node app.js
The app should be running on http://localhost:3000.