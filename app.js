const config = require('config');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
console.log("MONGODB_URI:", config.get('MONGODB_URI'));

const express = require('express');
const mongoose = require('mongoose');
const authRoutes = require('./routes/authRoutes');

const app = express();

// middleware
app.use(express.static('public'));

app.use(cookieParser());

// view engine
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// routes
app.get('/', (req, res) => res.render('home'));
app.get('/smoothies', (req, res) => res.render('smoothies'));
app.use(authRoutes);

// use authRoutes after defining app
app.use(authRoutes);

// database connection
// database connection
console.log("Connecting to MongoDB:", config.get('MONGODB_URI'));
mongoose.connect(config.get('MONGODB_URI'))
  .then(() => app.listen(3000, () => {
    console.log('Server is running on port 3000');
  }))
  .catch((err) => console.log(err));

